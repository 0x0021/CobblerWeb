from django.contrib import admin

# Register your models here.

from .models import *

class ScriptsAdmin(admin.ModelAdmin):
    list_display = ('zwname','qpname')
    list_per_page = 30
    search_fields = ('zwname','qpname')

admin.site.register(Scripts,ScriptsAdmin)


admin.site.register([initenv,])