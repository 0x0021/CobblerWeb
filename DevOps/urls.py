"""layui URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from . import views

urlpatterns = [
    path('',views.index,name='index'),
    path('welcome.html',views.welcome,name='welcome'),


    path('email-setting.html',views.email_setting,name='email_setting'),
    path('cobbler-setting.html', views.cobbler_setting, name='cobbler_setting'),


    path('user-add.html', views.user_add, name='user_add'),
    path('user-edit.html', views.user_edit, name='user_edit'),

    path('scripts.html',views.scripts,name="scripts"),
    path('scripts-edit.html', views.scripts_edit, name='scripts_edit'),
    path('scripts-add.html', views.scripts_add, name='scripts_add'),

    path('scripts-group.html', views.scripts_group, name="scripts_group"),
    path('scripts-group-edit.html', views.scripts_group_edit, name='scripts_group_edit'),
    path('scripts-group-add.html', views.scripts_group_add, name='scripts_group_add'),



    path('init-base-env.html', views.init_base_env, name='init-base-env'),
    path('edit-init-base-env.html', views.edit_init_base_env, name='edit-init-base-env'),

    path('add-remote-init-base-env.html', views.add_remote_init_base_env, name='add-remote-init-base-env'),
    path('add-init-base-env.html', views.add_init_base_env, name='add-init-base-env'),

    path('init-system-kickstarts.html', views.init_system_kickstart, name='init-system-kickstarts'),
    path('edit-system-kickstarts.html', views.edit_system_kickstart, name='edit-system-kickstarts'),
    path('add-system-kickstarts.html', views.add_system_kickstart, name='add-system-kickstarts'),

    path('init-system-snippets.html', views.init_system_snippets, name='init-system-snippets'),
    path('edit-system-snippets.html', views.edit_system_snippets, name='edit-system-snippets'),
    path('add-system-snippets.html', views.add_system_snippets, name='add-system-snippets'),

    path('init-system-system.html', views.init_system_system, name='init-system-system'),
    path('edit-system-system.html', views.edit_system_system, name='edit-system-system'),
    path('add-system-system.html', views.add_system_system, name='add-system-system'),

    path('init-system-task.html', views.init_system_task, name='init-system-task'),
    path('edit-system-task.html', views.edit_system_task, name='edit-system-task'),
    path('add-system-task.html', views.add_system_task, name='add-system-task'),
    path('info-system-task.html', views.info_system_task, name='info-system-task'),

    path('api/scripts/get', views.api_scripts_get, name='api_scripts_get'),
    path('api/scripts/add', views.api_scripts_add, name='api_scripts_add'),
    path('api/scripts/edit', views.api_scripts_edit, name='api_scripts_edit'),
    path('api/scripts/del', views.api_scripts_del, name='api_scripts_del'),

    path('api/scripts_group/get', views.api_scripts_group_get, name='api_scripts_group_get'),
    path('api/scripts_group/add', views.api_scripts_group_add, name='api_scripts_group_add'),
    path('api/scripts_group/edit', views.api_scripts_group_edit, name='api_scripts_group_edit'),
    path('api/scripts_group/del', views.api_scripts_group_del, name='api_scripts_group_del'),





    path('api/initbase/get',views.api_initbase_get,name='api_initbase_get'),
    path('api/initbase/add', views.api_initbase_add, name='api_initbase_add'),
    path('api/initbase/del', views.api_initbase_del, name='api_initbase_del'),

    path('api/remote/initbase/add', views.api_remote_initbase_add, name='api_remote_initbase_add'),



    path('api/inist_system_kickstart/get',views.api_init_system_kickstart_get,name='api_init_system_kickstart_get'),
    path('api/inist_system_kickstart/del', views.api_init_system_kickstart_del, name='api_init_system_kickstart_del'),
    path('api/inist_system_kickstart/edit', views.api_init_system_kickstart_edit, name='api_init_system_kickstart_edit'),
    path('api/inist_system_kickstart/add', views.api_init_system_kickstart_add,name='api_init_system_kickstart_add'),

    path('api/inist_system_snippets/get', views.api_init_system_snippets_get, name='api_init_system_snippets_get'),
    path('api/inist_system_snippets/del', views.api_init_system_snippets_del, name='api_init_system_snippets_del'),
    path('api/inist_system_snippets/edit', views.api_init_system_snippets_edit,
         name='api_init_system_snippets_edit'),
    path('api/inist_system_snippets/add', views.api_init_system_snippets_add, name='api_init_system_snippets_add'),


    path('api/inist_system_system/get', views.api_init_system_system_get, name='api_init_system_system_get'),
    path('api/inist_system_system/del', views.api_init_system_system_del, name='api_init_system_system_del'),
    path('api/inist_system_system/edit', views.api_init_system_system_edit,
         name='api_init_system_system_edit'),
    path('api/inist_system_system/add', views.api_init_system_system_add, name='api_init_system_system_add'),

    path('api/inist_system_task/get', views.api_init_system_task_get, name='api_init_system_task_get'),
    path('api/inist_system_task/del', views.api_init_system_task_del, name='api_init_system_task_del'),
    path('api/inist_system_task/edit', views.api_init_system_task_edit,
         name='api_init_system_task_edit'),
    path('api/inist_system_task/add', views.api_init_system_task_add, name='api_init_system_task_add'),
    path('api/inist_system_task/run', views.api_init_system_task_run, name='api_init_system_task_run'),

    # 主要负责webshell 获取主机登录信息，包含资产webshell,推送环境webshell
    path('api/hosts/get', views.api_hosts_get, name='api_hosts_get'),

    # 添加远端推送环境时候，验证 主机是否可以正常连
    path('api/hosts/check/online', views.api_hosts_check_online, name='api_hosts_check_online'),

    path('api/webshell/get', views.api_webshell_get, name='api_webshell_get'),


    path('api/setting/email/edit', views.api_setting_email_edit, name='api_email_setting_edit'),
    path('api/setting/email/get',views.api_setting_email_get,name='api_email_setting_get'),
    path('api/setting/email/test', views.api_setting_email_test, name='api_email_setting_test'),

    path('api/setting/cobbler/edit', views.api_setting_cobbler_edit, name='api_cobbler_setting_edit'),
    path('api/setting/cobbler/get', views.api_setting_cobbler_get, name='api_cobbler_setting_get'),
    path('api/setting/cobbler/test', views.api_setting_cobbler_test, name='api_cobbler_setting_test'),

]
