from django.db import models



class LoginLogs(models.Model):
    user = models.CharField(max_length=32, verbose_name='登录用户', null=True)
    ip = models.GenericIPAddressField(verbose_name='用户地址', null=True)
    ctime = models.DateTimeField(auto_now_add=True, verbose_name='时间')

    class Meta:
        db_table = "LoginLogs"
        verbose_name = "平台登录日志"
        verbose_name_plural = '平台登录日志'

    def __str__(self):
        return self.user



class ScriptsGroup(models.Model):
    groupname=models.CharField(max_length=100,verbose_name="脚本组名称",unique=True)
    status=models.IntegerField(default=1,verbose_name="脚本组状态")  # 1表示失效
    addtime = models.DateTimeField(auto_now_add=True, verbose_name="添加时间")
    path=models.CharField(max_length=1000,verbose_name="脚本路径",default="/root/.devops/")
    uptime = models.DateTimeField(auto_now_add=True, verbose_name="更新时间")
    remarks = models.CharField(max_length=2000, verbose_name="描述")


    def __str__(self):
        return self.groupname

    class Meta:
        verbose_name = '主机类型'
        verbose_name_plural = '主机类型'


class Scripts(models.Model):
    zwname = models.CharField(max_length=200, verbose_name="脚本名称[中文]", unique=True)
    qpname = models.CharField(max_length=200,verbose_name="脚本名称[全拼]",unique=True)
    context= models.TextField(max_length=200000,verbose_name="脚本内容")
    options = models.TextField(max_length=2000, verbose_name="使用声明")
    addtime = models.DateTimeField(auto_now_add=True, verbose_name="添加时间")
    uptime = models.DateTimeField(auto_now_add=True, verbose_name="更新时间")
    remarks = models.CharField(max_length=2000, verbose_name="描述")
    scriptsgroup = models.ForeignKey('ScriptsGroup', on_delete=False, default=1, verbose_name="脚本组")

    def __str__(self):
        return self.zwname

    class Meta:
        verbose_name = '脚本'
        verbose_name_plural = '脚本'



class initenv(models.Model):
    network_ip=models.GenericIPAddressField(verbose_name="主机 IP")
    username=models.CharField(max_length=200,verbose_name="登录用户")
    addtime = models.DateTimeField(auto_now_add=True, verbose_name="添加时间")
    password = models.CharField(max_length=200,verbose_name='登录密码')
    port = models.IntegerField(verbose_name="登录端口", default='65534', null=True, blank=True)
    scripts=models.ForeignKey('Scripts',verbose_name="推送脚本",on_delete=False)
    remarks = models.CharField(max_length=2000, verbose_name="描述")
    status=models.CharField(max_length=200,verbose_name="推送状态",default="已经添加任务")
    flag=models.IntegerField(verbose_name="添加任务状态",default=0) # 0表示资产添加，1表示远端添加




'''
cobbler add system 

{"name":system_info.get("name"),"interface":interface,
                                 "gateway":system_info.get("gateway"),"hostname":hostname,"mac_address":mac_address,
                                 "netmask":netmask,"ip_address":ip_address,
                                 "kernel_options":json.dumps(system_info.get("kernel_options")),
                                 "name_servers":system_info.get("name_servers"),
                                 "netboot_enabled":system_info.get("netboot_enabled"),
                                 "kickstart":system_info.get("kickstart"),"profile":system_info.get("profile")
                                 }
'''


class CobblerSystemInit(models.Model):
    hostname = models.CharField(max_length=1000, verbose_name='主机名称')
    interface = models.CharField(max_length=1000, verbose_name='网卡接口')
    gateway = models.CharField(max_length=1000, verbose_name='网关')
    mac_address = models.CharField(max_length=1000, verbose_name='MAC')
    netmask = models.CharField(max_length=1000, verbose_name='子网掩码')
    ip_address = models.CharField(max_length=1000, verbose_name='ip地址')
    kernel_options = models.CharField(max_length=1000, verbose_name='内核参数')
    name_servers = models.CharField(max_length=1000, verbose_name='dns地址')
    kickstart = models.CharField(max_length=1000, verbose_name='ks启动文件')
    netboot_enabled =models.BooleanField(verbose_name="激活",default=True)
    profile = models.CharField(max_length=1000, verbose_name='系统配置文件')
    password = models.CharField(max_length=200,verbose_name='root登录密码')

    starttime = models.CharField(max_length=100,verbose_name="开始安装时间",default="")
    endtime = models.CharField(max_length=100,verbose_name="结束安装时间",default="")
    successcount = models.IntegerField(verbose_name="成功安装系统的次数",default=0)
    installcount = models.IntegerField(verbose_name="本系统安装的总次数",default=0)
    status = models.CharField(max_length=100,verbose_name="系统安装状态",default="未推送系统")

    addtime = models.DateTimeField(auto_now_add=True, verbose_name="添加时间")
    uptime = models.DateTimeField(auto_now_add=True, verbose_name="更新时间")
    remarks = models.CharField(max_length=2000, verbose_name="描述")

    def __str__(self):
        return self.hostname

    class Meta:
        verbose_name = '系统安装记录'
        verbose_name_plural = '系统安装记录'

class CobblerServer(models.Model):
    name= models.CharField(max_length=256,unique=True,verbose_name="cobbler 服务器名称")
    host = models.GenericIPAddressField(verbose_name="ip地址")
    username = models.CharField(max_length=256,verbose_name="登录用户")
    password = models.CharField(max_length=256,verbose_name="登录密码")

class MailRecUser(models.Model):
    name= models.CharField(max_length=256,verbose_name="用户名称")
    mail = models.EmailField(verbose_name="邮箱",unique=True)
    EMAIL_SUBJECT_PREFIX = models.CharField(max_length=256, verbose_name="主题")


class MailSettings(models.Model):
    EMAIL_HOST = models.CharField(max_length=256,verbose_name="SMTP主机")
    EMAIL_PORT = models.IntegerField(verbose_name="SMTP端口",default=25)
    EMAIL_HOST_USER = models.CharField(max_length=256,verbose_name="SMTP账号")
    EMAIL_HOST_PASSWORD = models.CharField(max_length=256,verbose_name="SMTP密码")
    EMAIL_SUBJECT_PREFIX = models.CharField(max_length=256,verbose_name="主题")
    EMAIL_USE_SSL = models.BooleanField(default=0,verbose_name="SSL")
    EMAIL_USE_TLS = models.BooleanField(default=0,verbose_name="TLS")