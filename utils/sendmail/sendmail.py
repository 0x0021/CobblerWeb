#!/usr/bin/env python
# -*- coding:utf-8 -*-
# author : liuyu
# date : 2018/12/27 0027

from django.core.mail import send_mail
from DevOps import models
from django.conf import settings

def testmail(channposts):
    channposts["EMAIL_USE_SSL"]=1-int(channposts.get("EMAIL_USE_SSL",1))
    channposts["EMAIL_USE_TLS"]=1-int(channposts.get("EMAIL_USE_TLS",1))
    for k, v in channposts.items():
        if k.startswith('EMAIL'):
            setattr(settings, k, v)
    try:
        subject = "Cobbler Web smtp Test"
        message = "Test smtp setting"
        info=send_mail(subject, message, channposts["EMAIL_HOST_USER"], [channposts["EMAIL_HOST_USER"]])
        return info
    except Exception as e:
        return str(e)


