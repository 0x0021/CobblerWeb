#!/usr/bin/env python
# -*- coding:utf-8 -*-
# author : liuyu
# date : 2018/8/4 0004
from devops.models import AssetInfo
from utils.remote_exec_cmd import ansible_api


def update_hardware_infos(hostip):
    host_obj = AssetInfo.objects.filter(network_ip=hostip).first()
    sysinfos = ansible_api.getsysinfo([{"host": host_obj.network_ip, "port": host_obj.port, "user": host_obj.user.username, "password": host_obj.user.password}])
    print("{},{}".format("systeminfo===============================",sysinfos))
    for sysinfo in sysinfos:
        for k in sysinfo:
            sys=sysinfo[k]["message"]
            if sysinfo[k]["code"]==0:
                AssetInfo.objects.filter(network_ip=host_obj.network_ip).update(
                    system=sys['system'],
                    cpu=sys['cpu'],
                    memory=str(sys['mem']) + "G",
                    disk=sys['disk'],
                )
    return sysinfos
